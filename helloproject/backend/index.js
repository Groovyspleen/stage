import express from 'express';
import path from 'path';

const app = express();
const port = 3000;

const buildPath = path.join('/app/build');

app.use(express.static(buildPath));

app.get('/', (req, res) => {
  res.json({test:'Hello, World!'} );
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
